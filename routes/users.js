var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
//Rol
const RolController = require("../controls/RolController");
var rolController = new RolController();
//Persona
const PersonaController = require("../controls/PersonaController");
var personaController = new PersonaController();
//Auto
const AutoController = require("../controls/AutoController");
var autoController = new AutoController();
//Marca
const MarcaController = require("../controls/MarcaController");
var marcaController = new MarcaController();
//Cuenta
const CuentaController = require("../controls/CuentaController");
var cuentaController = new CuentaController();
//
let jwt = require('jsonwebtoken');

//middleWare: Es un filtrador, filtra las peticiones para si lo que se dice es verdad
//autentificacion
var auth = function middleWare(req, res, next) { //next: Dice si pasa el filtro, sigue a la accion siguiente, o sino te quita de ahi
  const token = req.headers['x-api-token']; //Decirle al usuario como debe llamarlo
  //console.log(req.headers);
  if (token) {
    require('dotenv').config();
    //Se obtiene la llave del archivo .env
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "Token no válido o expirado!", code: 401 });

      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (aux == null) {
          res.status(401);
          res.json({ msg: "Token no válido!", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token!", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

router.get('/roles', rolController.listar);
//personas
//post
router.post('/personas/guardar', [
  body('apellidos', 'Ingrese datos').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage('Ingrese un valor mayor o igual a 3, y menor a 100'),
  body('nombres', 'Ingrese datos').exists()
], personaController.guardar);
router.post('/personas/modificar', auth, personaController.modificar);
router.get('/personas', auth, personaController.listar);
router.get('/personas/obtener/:external', auth, personaController.obtener);

//autos
router.post('/auto/guardar',  autoController.guardar);
//post
router.post('/auto/modificar', autoController.modificar);
router.get('/auto', autoController.listar);
router.get('/marca', marcaController.listar);
router.get('/auto/obtener/:external', autoController.obtener);
//CUENTA
router.post('/sesion', [
  body('correo', 'Ingrese un correo valido').trim().exists().not().isEmpty(),
  body('clave', 'Ingrese una clave valida').exists().not().isEmpty()
], cuentaController.sesion);

/*router.get('/sumar/:a/:b', function(req, res, next) {
  var a = req.params.a * 1;
  var b = Number(req.params.b);
  var c = a +b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});
*/
router.post('/sumar', function (req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if (isNaN(a) || isNaN(b)) {
    res.status(400);
    res.json({ "msg": "Faltan Datos" });
  }
  console.log(b);
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });
});

module.exports = router;