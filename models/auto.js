'use strict';
module.exports = (sequalize, DataTypes) => {
    const auto = sequalize.define('auto', {
        nombre: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        modelo: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        color: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        precio: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        dueño_identificacion: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    auto.associate = function (models) {
        auto.belongsTo(models.marca, { foreignKey: 'id_marca' });
    }
    return auto;
}