'use strict';
module.exports = (sequalize, DataTypes) => {
    const factura = sequalize.define('factura', {
        nombreEmpresa: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        ruc: { type: DataTypes.STRING(20), unique: true, allowNull: false, defaultValue: "NO_DATA" },
        direccionEmpresa: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        correoEmpresa: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    factura.associate = function (models) {
        factura.hasOne(models.detalleFactura, { foreignKey: 'id_factura', as: 'detalleFactura' })
    }
    return factura;
}