'use strict';
module.exports = (sequalize, DataTypes) => {
    const persona = sequalize.define('persona', {
        nombres: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        apellidos: { type: DataTypes.STRING(50), defaultValue: "NO_DATA" },
        identificacion: { type: DataTypes.STRING(20), unique: true, allowNull: false, defaultValue: "NO_DATA" },
        tipo_identificacion: { type: DataTypes.ENUM("CEDULA", "PASAPORTE", "RUC"), allowNull: false, defaultValue: "CEDULA" },
        direccion: { type: DataTypes.STRING(255), allowNull: true, defaultValue: 'NO_DATA' },
        telefono: { type: DataTypes.STRING(20), defaultValue: 'NO_DATA' },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });
    persona.associate = function (models) {
        persona.belongsTo(models.rol, { foreignKey: 'id_rol' });
        persona.hasOne(models.cuenta, { foreignKey: 'id_persona', as: 'cuenta' });
    };
    return persona; x
}