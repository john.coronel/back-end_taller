'use strict';
module.exports = (sequalize, DataTypes) => {
    const detalleFactura = sequalize.define('detalleFactura', {
        unidades: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        descripcion: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        valorUnitario: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        valorTotal: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        iva: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        total: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    detalleFactura.associate = function (models) {
    }
    return detalleFactura;
}