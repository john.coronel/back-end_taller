'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var rol = models.rol;
var persona = models.persona;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8; //Puede ser cualquier número
class PersonaController {
    async listar(req, res) {
        //Averiguar para el míercoles cómo sacar el correo y mostrar solo el correo
        var lista = await persona.findAll({
            include: { model: models.cuenta, as: 'cuenta', attributes: ['correo'] },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'tipo_identificacion'] //lista los atributos que requiera 
        }); //
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external }, include: { model: models.cuenta },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'tipo_identificacion'] //lista los atributos que requiera 
        }); //
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let rol_id = req.body.external_rol; //req.body es para los post
            if (rol_id != undefined) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null); //el null deberia recibir un callback
                    };
                    var data = {
                        identificacion: req.body.dni, //en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
                        tipo_identificacion: req.body.tipo,
                        nombres: req.body.nombres,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        telefono: req.body.telefono,
                        id_rol: rolAux.id,
                        cuenta: {
                            correo: req.body.correo,
                            clave: claveHash(req.body.clave)
                        }
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                    console.log(transaction);
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var person = await persona.findOne({ where: { external_id: req.body.external } });
            if (person === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
                //TODO
            } else {
                var uuid = require('uuid');
                person.identificacion = req.body.dni;//en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
                person.tipo_identificacion = req.body.tipo;
                person.nombres = req.body.nombres;
                person.apellidos = req.body.apellidos;
                person.direccion = req.body.direccion;
                person.external_id = uuid.v4();
                var result = await person.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}
module.exports = PersonaController;