'use strict';
var models = require('../models/');
var marca = models.marca;
class MarcaContmarcaler {
    async listar(req, res) {
        var lista = await marca.findAll({
            attributes: ['nombre', 'external_id', 'estado']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }
}
module.exports = MarcaContmarcaler;