'use strict';

const { validationResult } = require("express-validator");
var models = require('../models/');
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8; //Puede ser cualquier número
let jwt = require('jsonwebtoken');

class CuentaController {
    async sesion(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty) {
            var login = await cuenta.findOne({
                where: { correo: req.body.correo },
                include: { model: models.persona, as: "persona", atributes: ['apellidos', 'nombres'] }
            });
            if (login === null) {
                res.status(400);
                res.json({ msg: "Cuenta no encontrada", code: 400, errors: errors });
            } else {
                //si encuentra el correo entra a esta parte
                res.status(200);

                var isClaveValida = function (clave, claveUser) {
                    return bcrypt.compareSync(claveUser, clave);
                };

                if (login.estado) {
                    //valido
                    if (isClaveValida(login.clave, req.body.clave)) {
                        //arreglo asociativo
                        const tokenData = {
                            external: login.external_id,
                            correo: login.correo,
                            check: true
                        };
                        require('dotenv').config();
                        //Se obtiene la llave del archivo .env
                        const llave = process.env.KEY;
                        //jwt.sign firma 
                        const token = jwt.sign(tokenData, llave, { expiresIn: '12h' });
                        res.json({
                            msg: "OK!",
                            code: 200,
                            token: token,
                            user: login.persona.nombres + '' + login.persona.apellidos,
                            correo: login.correo

                        });
                    } else {
                        res.json({ msg: "Clave incorrecta!", code: 400, errors: errors });
                    }
                } else {

                    res.json({ msg: "Cuenta desactivada", code: 400, errors: errors });

                }


            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}
module.exports = CuentaController;