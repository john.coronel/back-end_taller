'use strict';

const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var auto = models.auto;
var marca = models.marca;
const bcrypt = require('bcrypt');
const saltRounds = 8; //Puede ser cualquier número

class AutoController {
    async listar(req, res) {
        var lista = await auto.findAll({
            include: { model: models.marca, as: 'marca', attributes: ['nombre'] },
            attributes: ['nombre', 'modelo', 'color', 'precio', 'dueño_identificacion', 'estado']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        console.log("Esto es errors" + errors)
        if (errors.isEmpty()) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                var data = {
                    nombre: req.body.nombre,
                    modelo: req.body.modelo,
                    color: req.body.color,
                    dueño_identificacion: req.body.dueño_identificacion,
                    precio: req.body.precio,
                    id_marca: marcaAux.id,
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();
                try {
                    await auto.create(data, transaction);
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.errors && error.errors[0].message) {
                        res.json({ msg: error.errors[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
                console.log(transaction);
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            console.log("errors")
            res.status(400);
            res.json({ msg: "Faltan datos", code: 400 });
        }
    }

    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var aut = await auto.findOne({ where: { external_id: req.body.external } });
            if (aut === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
            } else {
                var uuid = require('uuid');
                aut.dueño_identificacion = req.body.dueño_identificacion;
                aut.nombre = req.body.nombre;
                aut.modelo = req.body.modelo;
                aut.color = req.body.color;
                aut.precio = req.body.precio;
                aut.external_id = uuid.v4();
                var result = await aut.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            } save
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async obtener(req, res) {
        const estado = req.params.estado;
        var lista = await auta.findOne({
            where: { estado: estado }, include: { model: models.marca },
            attributes: ['nombre', 'modelo', 'color', 'precio', 'dueño_identificacion', 'estado']
        });
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
}
module.exports = AutoController;